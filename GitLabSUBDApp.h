/***************************************************************
 * Name:      GitLabSUBDApp.h
 * Purpose:   Defines Application Class
 * Author:    Mihail Semchuk (semchuk937@gmail.com)
 * Created:   2019-03-23
 * Copyright: Mihail Semchuk ()
 * License:
 **************************************************************/

#ifndef GITLABSUBDAPP_H
#define GITLABSUBDAPP_H

#include <wx/app.h>

class GitLabSUBDApp : public wxApp
{
    public:
        virtual bool OnInit();
};

#endif // GITLABSUBDAPP_H
