/***************************************************************
 * Name:      GitLabSUBDMain.h
 * Purpose:   Defines Application Frame
 * Author:    Mihail Semchuk (semchuk937@gmail.com)
 * Created:   2019-03-23
 * Copyright: Mihail Semchuk ()
 * License:
 **************************************************************/

#ifndef GITLABSUBDMAIN_H
#define GITLABSUBDMAIN_H

//(*Headers(GitLabSUBDFrame)
#include <wx/frame.h>
#include <wx/menu.h>
#include <wx/statusbr.h>
//*)

class GitLabSUBDFrame: public wxFrame
{
    public:

        GitLabSUBDFrame(wxWindow* parent,wxWindowID id = -1);
        virtual ~GitLabSUBDFrame();

    private:

        //(*Handlers(GitLabSUBDFrame)
        void OnQuit(wxCommandEvent& event);
        void OnAbout(wxCommandEvent& event);
        //*)

        //(*Identifiers(GitLabSUBDFrame)
        static const long idMenuQuit;
        static const long idMenuAbout;
        static const long ID_STATUSBAR1;
        //*)

        //(*Declarations(GitLabSUBDFrame)
        wxStatusBar* StatusBar1;
        //*)

        DECLARE_EVENT_TABLE()
};

#endif // GITLABSUBDMAIN_H
