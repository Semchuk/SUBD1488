/***************************************************************
 * Name:      GitLabSUBDApp.cpp
 * Purpose:   Code for Application Class
 * Author:    Mihail Semchuk (semchuk937@gmail.com)
 * Created:   2019-03-23
 * Copyright: Mihail Semchuk ()
 * License:
 **************************************************************/

#include "GitLabSUBDApp.h"

//(*AppHeaders
#include "GitLabSUBDMain.h"
#include <wx/image.h>
//*)

IMPLEMENT_APP(GitLabSUBDApp);

bool GitLabSUBDApp::OnInit()
{
    //(*AppInitialize
    bool wxsOK = true;
    wxInitAllImageHandlers();
    if ( wxsOK )
    {
    	GitLabSUBDFrame* Frame = new GitLabSUBDFrame(0);
    	Frame->Show();
    	SetTopWindow(Frame);
    }
    //*)
    return wxsOK;

}
